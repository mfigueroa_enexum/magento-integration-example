<?php
/**
 * Created by PhpStorm.
 * User: ENEXUM - CMOLINA
 * Date: 11/01/2018
 * Time: 8:33
 */

require '../app/bootstrap.php';
require 'Magento.php';

$magento = new Magento();

/**
 * Ejemplo de Obtener producto por SKU`
 */

$product = $magento->getObjectManager()->create('Magento\Catalog\Model\ProductRepository')->get('sku00212');

echo $product->getId();

