<?php
/**
 * Created by PhpStorm.
 * User: ENEXUM - CMOLINA
 * Date: 04/01/2018
 * Time: 9:23
 */

class Magento
{
    protected $bootstrap;
    protected $utils;
    protected static $total = 0;

    protected $log;

    public function __construct()
    {
        $this->bootstrap      = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);

        $state = $this->getObjectManager()->get('Magento\Framework\App\State');
        $state->setAreaCode('adminhtml');
    }

    /**
     * @return \Magento\Framework\ObjectManagerInterface
     */
    public function getObjectManager()
    {
        return $this->bootstrap->getObjectManager();
    }

    /**
     * Obtiene la informacion de la tienda
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore()
    {
        return $this->getObjectManager()->get('Magento\Store\Model\StoreManagerInterface')->getStore();
    }
}