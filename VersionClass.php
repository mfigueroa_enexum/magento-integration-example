<?php
/**
 * Created by PhpStorm.
 * User: ENEXUM - CMOLINA
 * Date: 11/01/2018
 * Time: 8:35
 */

require '../app/bootstrap.php';
require 'Magento.php';

/**
 * Ejemplo de Obtener producto por SKU`
 */

class VersionClass extends Magento
{
    public function getProductId()
    {
        $product = $this->getObjectManager()->create('Magento\Catalog\Model\ProductRepository')->get('sku00212');

        return $product->getId();
    }
}

$version = new VersionClass();
echo $version->getProductId();